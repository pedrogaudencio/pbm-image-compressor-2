#!/usr/bin/env python
# -*- coding: latin-1 -*-
from sys import argv


if len(argv) != 2:
    print "Usage: python descomprimir.py <image.pbmz>" 
    exit()


def write_decompressed(decompressed):
    f = open(argv[1].replace(".pbmz","_z.pbm"),'w')
    f.write("".join(decompressed))
    f.close()


def decompress(f):
    bs = ["P4\n"]
    height, width, stream = [], [], []
    out = ""

    try:  
        byte = f.read(1)
        height.append('{0:08b}'.format(ord(byte)))
        while byte != '\x00':
            byte = f.read(1)
            height.append('{0:08b}'.format(ord(byte)))
        byte = f.read(1)
        width.append('{0:08b}'.format(ord(byte)))
        while byte != '\x00':
            byte = f.read(1)
            width.append('{0:08b}'.format(ord(byte)))
        byte = f.read(1)
        while byte != "":
            stream.append('{0:08b}'.format(ord(byte)))
            byte = f.read(1)
    finally:
        f.close()

    h, w = 0, 0

    for i in height:
        h += int(i, 2)

    for i in width:
        w += int(i, 2)

    for a in ["%d" % h, ' ', "%d" % w, '\n']:
        bs.append(a)

    for ch in stream:
        b = list(ch)
        color = b[0]
        b[0] = '0'
        out += color*int("".join(b), 2)

    while out != "":
        bs.append(chr(int(out[:8],2)))
        out = out[8:]

    write_decompressed(bs)


with open(argv[1], "rb") as f:
    decompress(f)
