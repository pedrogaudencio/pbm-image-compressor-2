#!/usr/bin/env python
# -*- coding: latin-1 -*-
from collections import Counter
from sys import argv
from math import log
from numpy.linalg import matrix_power
from numpy import array
import copy
import re


if len(argv) != 2: 
    print "Usage: python comprimir.py <image.pbm>" 
    exit()


def calculate_entropy(bits, r00, r11):
    entropia = 0.0
    c = Counter(bits)
    dictn = c.items()
    total_symbols = sum(c.values())

    freq = ()

    for key, value in dictn:
        freq += ((key, float(value)/total_symbols),)

    for key, value in freq:
        entropia -= value*log(value,2)

    print "entropia: %0.04f" % entropia, "bits"

    simbolos = [[0.0 for i in range(2)] for i in range(2)]

    r = ""

    for key2, value1 in dictn:
        for key1, value2 in dictn:
            if key1 != key2:
                r = "(" + re.escape("%c%c" % (key1, key2)) + ")"
                #print r, len([match.start() for match in re.finditer(r, bits)])
                simbolos[int(key1)][int(key2)] = float(len([match.start() for match in re.finditer(r, bits)]))/(total_symbols-1)
                #print r, ":", float(len([match.start() for match in re.finditer(r, f)]))/total_symbols
            else:
                if key1 == key2 == '0':
                    #r = "(" + re.escape("%c%c" % (key1, key2)) + ")"
                    simbolos[int(key1)][int(key2)] = float(r00)/(total_symbols-1)
                    #print r, r00
                elif key1 == key2 == '1':
                    #r = "(" + re.escape("%c%c" % (key1, key2)) + ")"
                    simbolos[int(key1)][int(key2)] = float(r11)/(total_symbols-1)
                    #print r, r11
    #print total_symbols

    simbolos_original = copy.deepcopy(simbolos)

    simbolos[0][0] = round(simbolos_original[0][0]/(simbolos_original[0][0]+simbolos_original[0][1]), 6)
    simbolos[0][1] = round(simbolos_original[0][1]/(simbolos_original[0][1]+simbolos_original[0][0]), 6)
    simbolos[1][0] = round(simbolos_original[1][0]/(simbolos_original[1][0]+simbolos_original[1][1]), 6)
    simbolos[1][1] = round(simbolos_original[1][1]/(simbolos_original[1][1]+simbolos_original[1][0]), 6)

    est = matrix_power(array(simbolos), 1000)

    #print "estacionários: ", est

    entropia_cond = - est[0][0] * (simbolos[0][0]*log(simbolos[0][0], 2) + simbolos[1][0]*log(simbolos[1][0], 2)) - est[0][1] * (simbolos[0][1]*log(simbolos[0][1], 2) + simbolos[1][1]*log(simbolos[1][1], 2))

    print "entropia condicional: %0.04f bits" % entropia_cond


def read_pbm(f):
    bits, header = "", ""
    try:  
        byte = f.read(1)
        header = byte
        while byte != "\n":
            byte = f.read(1)
            header += byte
        byte = f.read(1)
        header += byte
        while byte != "\n":
            byte = f.read(1)
            header += byte
        byte = f.read(1)
        while byte != "":
            bits += '{0:08b}'.format(ord(byte))
            byte = f.read(1)
    finally:
        f.close()

    compressed = handle_dimensions(header) + handle_bits(bits)
    write_compressed(compressed)


def handle_dimensions(header):
    header = re.sub("(#.*)", '', header) # remover comments
    header = header.replace('P4', '').replace('\n', ''). split(' ')

    dimensions = handle_size(int(header[0])) + handle_size(int(header[1]))
    to_char = []

    for c in dimensions:
        to_char.append("%c" % int(c, 2))

    return to_char


def handle_size(size):
    times, rest = 0, 0
    grouped = []

    if size > 255:
        times = size/255
        rest = size%255

        bit_len = "{0:08b}".format(255)
        grouped = [bit_len for t in range(times)]

        if rest > 0:
            grouped.append("{0:08b}".format(rest))

    else:
        grouped = ["".join("{0:08b}".format(size))]

    grouped.append("{0:08b}".format(0))

    return grouped


def handle_bits(bits):
    r0 = "^(0)+"
    r1 = "^(1)+"
    bits_to_group = ''
    compressed = []
    
    bits_original = bits

    r00 = 0
    r11 = 0

    while bits != "":
        match = re.match(r0, bits)
        if match:
            bits_to_group = match.group(0)
            bits = re.sub(r0, '', bits)
            compressed += convert_and_group(len(bits_to_group), '0')
            r00 += len(bits_to_group)-1

        else:
            match = re.match(r1, bits)
            if match:
                bits_to_group = match.group(0)
                bits = re.sub(r1, '', bits)
                compressed +=  convert_and_group(len(bits_to_group), '1')
                r11 += len(bits_to_group)-1

    calculate_entropy(bits_original, r00, r11)

    to_char = []

    for c in compressed:
        to_char.append("%c" % int(c, 2))

    return to_char


def convert_and_group(total_bit_len, color):
    times, rest = 0, 0
    grouped = []

    if total_bit_len > 127:
        times = total_bit_len/127
        rest = total_bit_len%127

        bit_len = "{0:08b}".format(127)
        b = list(bit_len)
        b[0] = color
        grouped = ["".join(b) for t in range(times)]

        if rest > 0:
            rest = "{0:08b}".format(rest)
            rest = list(rest)
            rest[0] = color
            grouped.append("".join(rest))
    
    else:
        times = "{0:08b}".format(total_bit_len)
        times = list(times)
        times[0] = color
        grouped = ["".join(times)]

    return grouped


def write_compressed(compressed):
    f = open(argv[1].replace(".pbm",".pbmz"),'w')
    f.write("".join(compressed))
    f.close()


with open(argv[1], "rb") as f:
    read_pbm(f)
