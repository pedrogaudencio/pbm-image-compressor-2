#!/usr/bin/env python
# -*- coding: latin-1 -*-
from collections import Counter
from sys import argv
from math import log
import re
import numpy

# exemplos:
se = numpy.array(([1,0],
                  [0,1]))

tc = numpy.array(([.9,.1],
                  [.1,.9]))

canalperdas = numpy.array(([.8,0.],
                           [.2,.2],
                           [0.,.8]))

maquina = numpy.array([(.5,.5,0.,0.,0.,0.,0.,0.),
                       (0.,.5,.5,0.,0.,0.,0.,0.),
                       (0.,0.,.5,.5,0.,0.,0.,0.),
                       (0.,0.,0.,.5,.5,0.,0.,0.),
                       (0.,0.,0.,0.,.5,.5,0.,0.),
                       (0.,0.,0.,0.,0.,.5,.5,0.),
                       (0.,0.,0.,0.,0.,0.,.5,.5),
                       (.5,0.,0.,0.,0.,0.,0.,.5)])

mq5 = numpy.array([(.5,0.,0.,0.,0.,.5),
                   (.5,.5,0.,0.,0.,0.),
                   (0.,.5,.5,0.,0.,0.),
                   (0.,0.,.5,.5,0.,0.),
                   (0.,0.,0.,.5,.5,0.),
                   (0.,0.,0.,0.,.5,.5)])


def actualizaPj(inPj,Cj):
    tam = len(inPj)
    Pj = numpy.zeros(tam) 
    soma = 0.
    for j in range(0,tam):  
        soma += inPj[j]*Cj[j]
    
    for j in range(0,tam):  
        Pj[j] = inPj[j] * (Cj[j]/soma)
        
    return Pj


def calculaIL(Pj,Cj):
    tam = len(Pj)
    soma = 0.
    IL = 0.
    for j in range(0, tam):
            soma += Pj[j]*Cj[j]
    IL = log(soma, 2)

    return IL


def calculaIU(Cj):
    IU = 0.
    IU = log(max(Cj), 2)

    return IU


def calculaCj(Qkj, Pj):
    tam = Qkj.shape
    tam1 = len(Pj)
    Cj = numpy.zeros(tam1)
    soma = 0.
    somatotal = 0.
    for j in range (0, tam1):
        somatotal = 0.
        for k in range(0, tam[0]):
            soma = 0.
            if Qkj[k][j] == 0:
                somatotal += 0
            else:   
                for i in range(0,tam[1]):
                    soma += Pj[i] * Qkj[k][i]
                somatotal += Qkj[k][j] * (numpy.log2(Qkj[k][j]/soma))
        Cj[j] = 2 ** (somatotal)
    return Cj


def capacidade(input, erro):
    resultado = blahut_runner(input, erro)

    print "\nCapacidade: %0.03f" % resultado, "bits\n"


def blahut_runner(Qkj,erro):
    #com tam = numero de colunas, encher Pj com 1/tam
    E = erro
    ta = Qkj.shape[1]
    Pj = numpy.zeros(ta)
    Pj.fill(1./ta)
    #Cj = numpy.zeros(ta)

    while True:
        Cj = calculaCj(Qkj,Pj)
        IL = calculaIL(Pj,Cj)
        IU = calculaIU(Cj)
        if(IU-IL<E):
            return IL
            break
        Pj = actualizaPj(Pj,Cj)


if len(argv) != 2:
    # testes com os exemplos dados acima
    erro = 1e-5

    print "Canal binário sem erros (erro: %0.4g):\n" % erro, se
    capacidade(se, erro)

    print "Canal binário com perdas (erro: %0.4g):\n" % erro, canalperdas
    capacidade(canalperdas, erro)

    print "Canal binário simétrico (erro: %0.4g):\n" % erro, tc
    capacidade(tc, erro)

    print "Máquina de escrever ruídosa (erro: %0.4g):\n" % erro, maquina
    capacidade(maquina, erro)
else:
    with open(argv[1], "r") as myfile:
        f = myfile.read().replace('\n', '')

    c = Counter(f)
    dictn = c.items()
    total_chars = sum(c.values())

    freq = ()

    for key, value in dictn:
        freq += ((key, float(value)/total_chars),)

    entropia = 0.0

    for key, value in freq:
        entropia -= value*log(value,2)

    #print "\n\n", dictn, "\n\n", "total: ", total_chars, "\n\n", "freq: ", freq
    print "Entropia: ", round(entropia, 2), "bits"

    caracteres = [[0.0 for i in range(255)] for i in range(255)]

    for key1, value1 in dictn:
        for key2, value2 in dictn:
            r = "(" + re.escape("%c%c" % (key1, key2)) + ")"
            caracteres[ord(key1)][ord(key2)] = float(len([match.start() for match in re.finditer(r, f)]))/total_chars

    caracteres = numpy.array(caracteres)

    print caracteres

    erro = 1e-5

    capacidade(caracteres, erro)
