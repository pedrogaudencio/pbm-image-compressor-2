from collections import Counter
from sys import argv
from math import log
import re
import numpy
 
if len(argv) != 2: 
    print "Usage: python counter.py <filename.txt>" 
    exit()

with open(argv[1], "r") as myfile:
    f = myfile.read().replace('\n', '')

c = Counter(f)
dictn = c.items()
total_chars = sum(c.values())

freq = ()

for key, value in dictn:
	freq += ((key, float(value)/total_chars),)

entropia = 0.0

for key, value in freq:
	entropia -= value*log(value,2)

#print "\n\n", dictn, "\n\n", "total: ", total_chars, "\n\n", "freq: ", freq
print "entropia: ", round(entropia, 2), "bits"

caracteres = [[0.0 for i in range(255)] for i in range(255)]

for key1, value1 in dictn:
	for key2, value2 in dictn:
		r = "(" + re.escape("%c%c" % (key1, key2)) + ")"
		caracteres[ord(key1)][ord(key2)] = float(len([match.start() for match in re.finditer(r, f)]))/total_chars
		#print r, ":", float(len([match.start() for match in re.finditer(r, f)]))/total_chars

#for item in words:
#	print ''.join(map(str, item[0:]))
"""
entropia_cond = 0.0

marginais = [sum(i) for i in zip(*caracteres)]

for value in marginais:
	if value > 0.0:
		entropia_cond -= value*log(value,2)

print "entropia condicional: ", entropia_cond, "bits"
"""

print caracteres

caracteres = numpy.array(caracteres)

print caracteres
